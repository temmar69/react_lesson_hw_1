import React, { Component } from 'react';
import Title from '../title';
import AddTask from '../add-task';

import './main.css'

class Main extends Component {
    state = { taskArr: [] }
    onUpDateTaskArr = (object) => {
        this.setState(
            ({ taskArr }) => { //тіло фуnкції
                const newArr = taskArr;
                newArr.push(object)
                return {
                    taskArr: newArr
                }
            }
        )
    } 
    onDeleteTask(index) {
        this.state.taskArr.splice(index, 1);
        this.setState(this.state);
    }
    onEditTaskStart(index) {
        this.state.taskArr[index].isOnEdit = true;
        this.setState(this.state);
    }
    onEditTaskEnd(index) {
        this.state.taskArr[index].isOnEdit = false;
        this.setState(this.state);
    }
    onEditTaskSave(index, value) {
        this.state.taskArr[index].text = value;
        this.setState(this.state);
    }
    render() {
        const { taskArr } = this.state;
        console.log(taskArr);
        return (
            <div className='main_div'>
                <Title text='Список задач на сегодня'> </Title>
                <AddTask 
                    taskArr={taskArr}
                    onUpDateTaskArr={this.onUpDateTaskArr}
                    onDeleteTask={this.onDeleteTask.bind(this)}
                    onEditTaskStart={this.onEditTaskStart.bind(this)}
                    onEditTaskEnd={this.onEditTaskEnd.bind(this)}
                    onEditTaskSave={this.onEditTaskSave.bind(this)}
                    ></AddTask>
                <Title text={`Осталось ${taskArr.length} задач`}> </Title>
            </div>
        )
    }
}
export default Main;
