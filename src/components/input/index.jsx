import React, { Component } from 'react';

import "./input.css";

class Input extends Component {

    /*   constructor() {
        super();
        this.saveOnEnter = this.saveOnEnter.bind(this);
    }
    */
    state = {
        inptInfo: ""
    }
    addText = (text) => {
        this.setState(() => {
            return this.state.inptInfo = text
        })
    }
    saveBtn() {
        //const a = this.state.inptInfo.length < 4 ? null : this.props.onUpDateTaskArr({ text: this.state.inptInfo, date: new Date(), active: true });
        if (this.state.inptInfo.length >= 4) {
            this.props.onUpDateTaskArr({ text: this.state.inptInfo, date: new Date(), active: true, isOnEdit: false })
        }
    }
    saveOnEnter(event) {
        if (event.key === 'Enter') {
            this.saveBtn()
        }
    }
    render() {
        return (
            <div className='task-area'>
                <input type='text' className="input-task"
                    onInput={(e) => this.addText(e.target.value)}
                    onKeyUp={this.saveOnEnter.bind(this)}
                />
                <input type='button' value='Сохранить' className="btn-save"
                    onClick={() => {
                        this.saveBtn()
                    }} />
            </div>
        )
    }
}

/*const Input = () => {
    

    return(
        <div>
            <input type='text' className="input-task" onInput={(e)=>{
                   addText(e.target.value)
            }}></input>
            <input type='button' value='Сохранить'className="btn-save" onClick={()=>{
                
            }}></input>
            <span>{console.log(inptInfo)}</span>
        </div>
    )
}*/

export default Input;