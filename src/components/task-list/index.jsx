import React from 'react';

import './task-list.css';

const TaskList = ({ taskArr, onDeleteTask, onEditTaskStart, onEditTaskEnd, onEditTaskSave }) => {
    return (
        <div>
            <ul className='all-task-lists'> 
                {taskArr.map((task, index) => {
                    return (
                        <li className="task-list" key={index * 5 + 'b'}>
                            <span style={task.isOnEdit ? {display: 'none'}: {}}>{task.text},{task.date.getDate()}</span>
                            <input style={task.isOnEdit ? {} : {display: 'none'} } value={task.text} onInput={(e) => onEditTaskSave(index, e.target.value)}/>
                            <div className='pencil' onClick={() => task.isOnEdit ? onEditTaskEnd(index) : onEditTaskStart(index)}><i className="fas fa-pen"></i></div>
                            <div className='delete' onClick={()=> onDeleteTask(index)}><i className="far fa-trash-alt"></i></div>
                        </li>)
                })}
            </ul>
        </div>
    )
}

export default TaskList;