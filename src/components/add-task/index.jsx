import React from 'react';
import Input from '../input';
import TaskList from '../task-list';


export default function AddTask({onUpDateTaskArr, taskArr, onDeleteTask, onEditTaskStart, onEditTaskEnd, onEditTaskSave}){
    return(
        <div>
            <Input onUpDateTaskArr = {onUpDateTaskArr}/>
            <TaskList
             taskArr={taskArr}
             onDeleteTask={onDeleteTask}
             onEditTaskStart={onEditTaskStart}
             onEditTaskEnd={onEditTaskEnd}
             onEditTaskSave={onEditTaskSave}
             ></TaskList>
        </div>
    )
} 